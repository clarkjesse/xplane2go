package display

/*
#cgo CFLAGS: -DIBM -DSIMDATA_EXPORTS -DXPLM200=1 -DXPLM210=1 -DXPLM300=1 -DXPLM301=1
#cgo CFLAGS: -I../../SDK/CHeaders
#cgo LDFLAGS: -L../../SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64
#include <XPLM/XPLMDisplay.h>
#include <stdlib.h>
#include <string.h>

extern void drawWindowCallback(XPLMWindowID inWindowID, void* inRefcon);
extern int windowHandleKey(XPLMWindowID inWindowID,char inChar,XPLMKeyFlags inFlags, char inVirtualKey, void* inRefcon, int losingFocus);
extern int windowHandleMouseClick(XPLMWindowID inWindowID, int x, int y, XPLMMouseStatus inMouse, void* inRefcon);
extern XPLMCursorStatus windowHandleCursorStatus(XPLMWindowID inWindowID, int x, int y, void* inRefcon);
extern int windowHandleMouseWheel(XPLMWindowID inWindowID,int x, int y, int wheel, int clicks, void* inRefcon);


*/
import "C"
import (
	"bitbucket.org/clarkjesse/xplane2go/api"
	"bitbucket.org/clarkjesse/xplane2go/api/utilities"
	"unsafe"
)

type MouseStatus int
type CursorStatus int
type WindowPositioningMode int
type WindowID unsafe.Pointer

type DrawWindow func(id WindowID, ref interface{})
type HandleKey func(id WindowID, keyCode api.KeyCode, flags api.KeyFlags, virtualKeyCode api.VirtualKeyCode, ref interface{}, losingFocus bool) int
type HandleMouseClick func(id WindowID, x, y int, status MouseStatus, ref interface{}) int
type HandleCursorStatus func(id WindowID, x, y int, ref interface{}) CursorStatus
type HandleMouseWheel func(id WindowID, x, y, wheel, clicks int, ref interface{}) int

type CreateWindowData struct {
	Left                 int
	Top                  int
	Right                int
	Bottom               int
	Visible              bool
	DrawWindowFunc       DrawWindow
	HandleMouseClickFunc HandleMouseClick
	HandleKeyFunc        HandleKey
	HandleCursorFunc     HandleCursorStatus
	HandleMouseWheelFunc HandleMouseWheel
	Ref                  interface{}
}

const (
	MouseDown MouseStatus = 1
	MouseDrag MouseStatus = 2
	MouseUp   MouseStatus = 3
)

const (
	CursorDefault CursorStatus = 0
	CursorHidden  CursorStatus = 1
	CursorArrow   CursorStatus = 2
	CursorCustom  CursorStatus = 3
)

const (
	WindowPositionFree 				WindowPositioningMode = 0
	WindowCenterOnMonitor 			WindowPositioningMode = 1
	WindowFullScreenOnMonitor 		WindowPositioningMode = 2
	WindowFullScreenOnAllMonitors 	WindowPositioningMode = 3
	WindowPopOut 					WindowPositioningMode = 4
	WindowVR 						WindowPositioningMode = 5 // Since XPLM301
)

var windows = make(map[*C.char]*CreateWindowData)

func GetScreenSize() (width, height int) {
	C.XPLMGetScreenSize((*C.int)(unsafe.Pointer(&width)), (*C.int)(unsafe.Pointer(&height)))
	return
}

func GetMouseLocation() (x, y int) {
	C.XPLMGetMouseLocation((*C.int)(unsafe.Pointer(&x)), (*C.int)(unsafe.Pointer(&y)))
	return
}

//export drawWindowCallback
func drawWindowCallback(windowId C.XPLMWindowID, ref unsafe.Pointer) {
	id := (*C.char)(ref)
	regInfo := windows[id]
	regInfo.DrawWindowFunc(WindowID(windowId), regInfo.Ref)
}

//export windowHandleKey
func windowHandleKey(windowId C.XPLMWindowID, char C.char, flags C.XPLMKeyFlags, virtualKey C.char, ref unsafe.Pointer, losingFocus C.int) C.int {
	id := (*C.char)(ref)
	regInfo := windows[id]
	return C.int(regInfo.HandleKeyFunc(WindowID(windowId), api.KeyCode(char), api.KeyFlags(flags), api.VirtualKeyCode(virtualKey), regInfo.Ref, losingFocus == 1))
}

//export windowHandleMouseClick
func windowHandleMouseClick(windowId C.XPLMWindowID, x, y C.int, mouse C.XPLMMouseStatus, ref unsafe.Pointer) C.int {
	id := (*C.char)(ref)
	regInfo := windows[id]
	return C.int(regInfo.HandleMouseClickFunc(WindowID(windowId), int(x), int(y), MouseStatus(mouse), regInfo.Ref))
}

//export windowHandleCursorStatus
func windowHandleCursorStatus(windowId C.XPLMWindowID, x, y C.int, ref unsafe.Pointer) C.XPLMCursorStatus {
	id := (*C.char)(ref)
	regInfo := windows[id]
	return C.XPLMCursorStatus(regInfo.HandleCursorFunc(WindowID(windowId), int(x), int(y), regInfo.Ref))
}

//export windowHandleMouseWheel
func windowHandleMouseWheel(windowId C.XPLMWindowID, x, y, wheel, clicks C.int, ref unsafe.Pointer) C.int {
	id := (*C.char)(ref)
	regInfo := windows[id]
	return C.int(regInfo.HandleMouseWheelFunc(WindowID(windowId), int(x), int(y), int(wheel), int(clicks), regInfo.Ref))
}

func CreateWindow(left, top, right, bottom int, isVisible bool, drawCallback DrawWindow, keyCallback HandleKey, mouseClickCallback HandleMouseClick, ref interface{}) WindowID {
	regInfo := &CreateWindowData{}
	regInfo.Left = left
	regInfo.Top = top
	regInfo.Right = right
	regInfo.Bottom = bottom
	regInfo.Visible = isVisible
	regInfo.DrawWindowFunc = drawCallback
	regInfo.HandleKeyFunc = keyCallback
	regInfo.HandleMouseClickFunc = mouseClickCallback
	regInfo.Ref = ref

	id := C.CString(utilities.IdGenerator())
	windows[id] = regInfo

	return WindowID(C.XPLMCreateWindow(
		C.int(left),
		C.int(top),
		C.int(right),
		C.int(bottom),
		C.int(utilities.FromBoolToInt(isVisible)),
		C.XPLMDrawWindow_f(unsafe.Pointer(C.drawWindowCallback)),
		C.XPLMHandleKey_f(unsafe.Pointer(C.windowHandleKey)),
		C.XPLMHandleMouseClick_f(unsafe.Pointer(C.windowHandleMouseClick)),
		unsafe.Pointer(id)))
}

func GetScreenBoundsGlobal() (left, top, right, bottom int) {
	C.XPLMGetScreenBoundsGlobal((*C.int)(unsafe.Pointer(&left)), (*C.int)(unsafe.Pointer(&top)), (*C.int)(unsafe.Pointer(&right)), (*C.int)(unsafe.Pointer(&bottom)))
	return
}

func CreateWindowEx(params *CreateWindowData) WindowID {
	id := C.CString(utilities.IdGenerator())
	defer C.free(unsafe.Pointer(id))
	windows[id] = params
	cCreateWindowData := C.XPLMCreateWindow_t{}
	cCreateWindowData.structSize = C.int(unsafe.Sizeof(cCreateWindowData))
	cCreateWindowData.left = C.int(params.Left)
	cCreateWindowData.top = C.int(params.Top)
	cCreateWindowData.right = C.int(params.Right)
	cCreateWindowData.bottom = C.int(params.Bottom)
	cCreateWindowData.visible = C.int(utilities.FromBoolToInt(params.Visible))
	cCreateWindowData.drawWindowFunc = C.XPLMDrawWindow_f(unsafe.Pointer(C.drawWindowCallback))
	cCreateWindowData.handleMouseClickFunc = C.XPLMHandleMouseClick_f(unsafe.Pointer(C.windowHandleMouseClick))
	cCreateWindowData.handleKeyFunc = C.XPLMHandleKey_f(unsafe.Pointer(C.windowHandleKey))
	cCreateWindowData.handleCursorFunc = C.XPLMHandleCursor_f(unsafe.Pointer(C.windowHandleCursorStatus))
	cCreateWindowData.handleMouseWheelFunc = C.XPLMHandleMouseWheel_f(unsafe.Pointer(C.windowHandleMouseWheel))
	cCreateWindowData.refcon = unsafe.Pointer(id)
	cCreateWindowData.layer = C.xplm_WindowLayerFloatingWindows
	cCreateWindowData.decorateAsFloatingWindow = 1
	return WindowID(C.XPLMCreateWindowEx((*C.XPLMCreateWindow_t)(unsafe.Pointer(&cCreateWindowData))))
}

func DestroyWindow(windowId WindowID) {
	C.XPLMDestroyWindow(C.XPLMWindowID(windowId))
}

func GetWindowGeometry(windowId WindowID) (left, top, right, bottom int) {
	C.XPLMGetWindowGeometry(
		C.XPLMWindowID(windowId),
		(*C.int)(unsafe.Pointer(&left)),
		(*C.int)(unsafe.Pointer(&top)),
		(*C.int)(unsafe.Pointer(&right)),
		(*C.int)(unsafe.Pointer(&bottom)))
	return
}

func SetWindowGeometry(windowId WindowID, left, top, right, bottom int) {
	C.XPLMSetWindowGeometry(C.XPLMWindowID(windowId), C.int(left), C.int(top), C.int(right), C.int(bottom))
}

func GetWindowIsVisible(windowId WindowID) bool {
	return C.XPLMGetWindowIsVisible(C.XPLMWindowID(windowId)) == 1
}

func SetWindowIsVisible(windowId WindowID, isVisible bool) {
	C.XPLMSetWindowIsVisible(C.XPLMWindowID(windowId), C.int(utilities.FromBoolToInt(isVisible)))
}

func GetWindowRefCon(windowId WindowID) interface{} {
	id := (*C.char)(C.XPLMGetWindowRefCon(C.XPLMWindowID(windowId)))
	return windows[id].Ref
}

func SetWindowRefCon(windowId WindowID, ref interface{}) {
	id := (*C.char)(C.XPLMGetWindowRefCon(C.XPLMWindowID(windowId)))
	windows[id].Ref = ref
}

func TakeKeyboardFocus(windowId WindowID) {
	C.XPLMTakeKeyboardFocus(C.XPLMWindowID(windowId))
}

func BringWindowToFront(windowId WindowID) {
	C.XPLMBringWindowToFront(C.XPLMWindowID(windowId))
}

func IsWindowInFront(windowId WindowID) bool {
	return C.XPLMIsWindowInFront(C.XPLMWindowID(windowId)) == 1
}

func SetWindowPositioningMode(
	windowId WindowID,
	positioningMode WindowPositioningMode,
	monitorIndex int)  {
	C.XPLMSetWindowPositioningMode(
		C.XPLMWindowID(windowId),
		C.int(positioningMode),
		C.int(monitorIndex))
}

func SetWindowGravity(
	windowId WindowID,
	leftGravity float64,
	topGravity float64,
	rightGravity float64,
	bottomGravity float64)  {
	C.XPLMSetWindowGravity(
		C.XPLMWindowID(windowId),
		C.float(leftGravity),
		C.float(topGravity),
		C.float(rightGravity),
		C.float(bottomGravity))
}

func SetWindowResizingLimits(
	windowId WindowID,
	minWidthBoxels int,
	minHeightBoxels int,
	maxWidthBoxels int,
	maxHeightBoxels int)  {
	C.XPLMSetWindowResizingLimits(
		C.XPLMWindowID(windowId),
		C.int(minWidthBoxels),
		C.int(minHeightBoxels),
		C.int(maxWidthBoxels),
		C.int(maxHeightBoxels))
}

func SetWindowTitle(windowId WindowID, windowTitle string) {
	cTitle := C.CString(windowTitle)
	defer C.free(unsafe.Pointer(cTitle))

	C.XPLMSetWindowTitle(C.XPLMWindowID(windowId), cTitle)
}

func WindowIsPoppedOut(windowId WindowID) bool {
	return C.XPLMWindowIsPoppedOut(C.XPLMWindowID(windowId)) == 1
}