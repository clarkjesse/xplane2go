package scenery

/*
#cgo CFLAGS: -DIBM -DSIMDATA_EXPORTS -DXPLM200=1 -DXPLM210=1 -DXPLM300=1 -DXPLM301=1
#cgo CFLAGS: -I../../SDK/CHeaders
#cgo LDFLAGS: -L../../SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64
#include <XPLM/XPLMScenery.h>
#include <stdlib.h>
#include <string.h>

extern void objectLoadedCallback(XPLMObjectRef inObject, void* inRefcon);
*/
import "C"
import (
	"bitbucket.org/clarkjesse/xplane2go/api/graphics"
	"bitbucket.org/clarkjesse/xplane2go/api/utilities"
	"unsafe"
)

type ObjectRef unsafe.Pointer

type DrawInfo struct {
	structSize int32
	x          float32
	y          float32
	z          float32
	pitch      float32
	heading    float32
	roll       float32
}

var objLoadedCallbacks = make(map[*C.char]*objectLoadedReg)

type ObjectLoaded func(objRef ObjectRef, ref interface{})

type objectLoadedReg struct {
	callback ObjectLoaded
	ref      interface{}
}

func LoadObject(path string) ObjectRef {
	cPath := C.CString(path)
	defer C.free(unsafe.Pointer(cPath))
	return ObjectRef(C.XPLMLoadObject(cPath))
}

//export objectLoadedCallback
func objectLoadedCallback(objRef C.XPLMObjectRef, ref unsafe.Pointer) {
	id := (*C.char)(ref)
	regInfo, _ := objLoadedCallbacks[id]
	regInfo.callback(ObjectRef(objRef), regInfo.ref)
	delete(objLoadedCallbacks, id)
	C.free(unsafe.Pointer(id))
}

func LoadObjectAsync(path string, callback ObjectLoaded, ref interface{}) {
	cPath := C.CString(path)
	defer C.free(unsafe.Pointer(cPath))
	id := C.CString(utilities.IdGenerator())
	regInfo := &objectLoadedReg{callback, ref}
	objLoadedCallbacks[id] = regInfo
	C.XPLMLoadObjectAsync(cPath, C.XPLMObjectLoaded_f(unsafe.Pointer(C.objectLoadedCallback)), unsafe.Pointer(id))
}

func DrawObjects(objRef ObjectRef, count int, positions []DrawInfo, lighting, earthRelative bool) {
	if len(positions) == 0 {
		C.XPLMDrawObjects(C.XPLMObjectRef(objRef), C.int(count), nil, C.int(utilities.FromBoolToInt(lighting)), C.int(utilities.FromBoolToInt(earthRelative)))
	} else {
		C.XPLMDrawObjects(C.XPLMObjectRef(objRef), C.int(count), (*C.XPLMDrawInfo_t)(unsafe.Pointer(&positions[0])), C.int(utilities.FromBoolToInt(lighting)), C.int(utilities.FromBoolToInt(earthRelative)))
	}
}

func UnloadObject(objRef ObjectRef) {
	C.XPLMUnloadObject(C.XPLMObjectRef(objRef))
}

func NewDrawInfo(lat, lon, alt, pitch, heading, roll float32) DrawInfo {
	x, y, z := graphics.WorldToLocal(float64(lat), float64(lon), float64(alt))
	return NewDrawInfoLocal(float32(x), float32(y), float32(z), pitch, heading, roll)
}

func NewDrawInfoLocal(x, y, z, pitch, heading, roll float32) DrawInfo {
	result := DrawInfo{0, x, y, z, pitch, heading, roll}
	result.structSize = int32(unsafe.Sizeof(result))
	return result
}
