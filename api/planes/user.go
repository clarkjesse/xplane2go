package planes

/*
#cgo CFLAGS: -DIBM -DSIMDATA_EXPORTS -DXPLM200=1 -DXPLM210=1 -DXPLM300=1 -DXPLM301=1
#cgo CFLAGS: -I../../SDK/CHeaders
#cgo LDFLAGS: -L../../SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64
#include <XPLM/XPLMPlanes.h>
#include <stdlib.h>
#include <string.h>
*/
import "C"
import "unsafe"

func SetUsersAircraft(aircraft string) {
	cPath := C.CString(aircraft)
	defer C.free(unsafe.Pointer(cPath))
	C.XPLMSetUsersAircraft(cPath)
}

func PlaceUserAtAirport(airportCode string) {
	cAirportCode := C.CString(airportCode)
	defer C.free(unsafe.Pointer(cAirportCode))
	C.XPLMPlaceUserAtAirport(cAirportCode)
}
