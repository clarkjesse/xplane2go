package logging

/*
#cgo CFLAGS: -DIBM -DSIMDATA_EXPORTS -DXPLM200=1 -DXPLM210=1 -DXPLM300=1 -DXPLM301=1
#cgo CFLAGS: -I../../SDK/CHeaders
#cgo LDFLAGS: -L../../SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64
#include <XPLM/XPLMUtilities.h>
#include <stdlib.h>
#include <string.h>

*/
import "C"
import (
	"fmt"
	"strings"
)

type Level struct {
	number byte
	name   string
}

var (
	Trace_Level = Level{1, "TRACE"}
	Debug_Level = Level{2, "DEBUG"}
	Info_Level = Level{3, "INFO"}
	Warning_Level = Level{4, "WARNING"}
	Error_Level = Level{5, "ERROR"}
	MinLevel = Info_Level
	PluginName = "<unknown>"
)

func GetLevelFromString(level string) Level {
	switch strings.ToUpper(level) {
	case "TRACE":
		return Trace_Level
	case "DEBUG":
		return Debug_Level
	case "INFO":
		return Info_Level
	case "WARNING":
		return Warning_Level
	case "ERROR":
		return Error_Level
	default:
		return Info_Level
	}
}

func Trace(msg string) {
	writeMessage(Trace_Level, msg)
}

func Tracef(format string, a... interface{}) {
	if Trace_Level.number >= MinLevel.number {
		Trace(fmt.Sprintf(format, a...))
	}
}

func Debug(msg string) {
	writeMessage(Debug_Level, msg)
}

func Debugf(format string, a... interface{}) {
	if Debug_Level.number >= MinLevel.number {
		Debug(fmt.Sprintf(format, a...))
	}
}

func Info(msg string) {
	writeMessage(Info_Level, msg)
}

func Infof(format string, a... interface{}) {
	if Info_Level.number >= MinLevel.number {
		Info(fmt.Sprintf(format, a...))
	}
}

func Warning(msg string) {
	writeMessage(Warning_Level, msg)
}

func Warningf(format string, a... interface{}) {
	if Warning_Level.number >= MinLevel.number {
		Warning(fmt.Sprintf(format, a...))
	}
}

func Error(msg string) {
	writeMessage(Error_Level, msg)
}

func Errorf(format string, a... interface{}) {
	if Error_Level.number >= MinLevel.number {
		Error(fmt.Sprintf(format, a...))
	}
}

func writeMessage(level Level, msg string) {
	if level.number >= MinLevel.number {
		debugString(fmt.Sprintf("[%v %v]: %v\n", strings.ToUpper(PluginName), level.name, msg))
	}
}

func debugString(msg string) {
	cMsg := C.CString(msg)
	C.XPLMDebugString(cMsg)
}