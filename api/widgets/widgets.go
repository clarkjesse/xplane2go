package widgets

/*
#cgo CFLAGS: -DIBM -DSIMDATA_EXPORTS -DXPLM200=1 -DXPLM210=1 -DXPLM300=1 -DXPLM301=1
#cgo CFLAGS: -I../../SDK/CHeaders/XPLM -I../../SDK/CHeaders/Widgets
#cgo LDFLAGS: -L../../SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64
#include <XPWidgets.h>
#include <XPStandardWidgets.h>
#include <XPLMUtilities.h>
#include <stdlib.h>
#include <string.h>

extern int widgetCallback(XPWidgetMessage inMessage, XPWidgetID inWidget, intptr_t inParam1, intptr_t inParam2);
 */
import "C"
import (
	"bitbucket.org/clarkjesse/xplane2go/api/display"
	"bitbucket.org/clarkjesse/xplane2go/api/utilities"
	"unsafe"
)

//API REFERENCE: https://developer.x-plane.com/sdk/XPStandardWidgets/

type WidgetID unsafe.Pointer

type WidgetCallback func(message WidgetMessage, widgetId WidgetID, param1, param2 int) int

type WidgetPropertyID int
type WidgetClass int
type WidgetMessage int

const (
	MainWindow 			WidgetClass = 1
	SubWindow 			WidgetClass = 2
	Button   			WidgetClass = 3
	TextField   		WidgetClass = 4
	ScrollBar   		WidgetClass = 5
	Caption   			WidgetClass = 6
	GeneralGraphics   	WidgetClass = 7
	Progress   			WidgetClass = 8
)

const (
	Property_Refcon						 WidgetPropertyID = 0 // A window's refcon is an opaque value used by client code to find other data based on it.
	Property_Dragging					 WidgetPropertyID = 1 // These properties are used by the utlities to implement dragging.
	Property_DragXOff					 WidgetPropertyID = 2
	Property_DragYOff					 WidgetPropertyID = 3
	Property_Hilited					 WidgetPropertyID = 4 // Is the widget hilited? (For widgets that support this kind of thing.)
	Property_Object						 WidgetPropertyID = 5 // Is there a C++ object attached to this widget?
	Property_Clip						 WidgetPropertyID = 6 // If this property is 1, the widget package will use OpenGL to restrict drawing to the Wiget's exposed rectangle.
	Property_Enabled					 WidgetPropertyID = 7 // Is this widget enabled (for those that have a disabled state too)?
	Property_SubWindowType			 	 WidgetPropertyID = 1200 // This property specifies the type of window. Set to one of the subwindow types above.
	Property_MainWindowHasCloseBoxes 	 WidgetPropertyID = 1200 // This property specifies whether the main window has close boxes in its corners.
	Property_ButtonType					 WidgetPropertyID = 1300 // This property sets the visual type of button. Use one of the button types above.
	Property_TextFieldType				 WidgetPropertyID = 1403 // This is the type of text field to display, from the above list.
	Property_UserStart					 WidgetPropertyID = 10000 // NOTE: Property IDs 1 - 999 are reserved for the widgets library.
	// Property IDs 1000 - 9999 are allocated to the standard widget classes provided with the library.
	// Properties 1000 - 1099 are for widget class 0, 1100 - 1199 for widget class 1, etc.
)

const (
	WidgetMsg_None					 WidgetMessage = 0
	WidgetMsg_Create				 WidgetMessage = 1
	WidgetMsg_Destroy				 WidgetMessage = 2
	WidgetMsg_Paint					 WidgetMessage = 3
	WidgetMsg_Draw 					 WidgetMessage = 4
	WidgetMsg_KeyPress				 WidgetMessage = 5
	WidgetMsg_KeyTakeFocus			 WidgetMessage = 6
	WidgetMsg_KeyLoseFocus			 WidgetMessage = 7
	WidgetMsg_MouseDown				 WidgetMessage = 8
	WidgetMsg_MouseDrag				 WidgetMessage = 9
	WidgetMsg_MouseUp				 WidgetMessage = 10
	WidgetMsg_Reshape				 WidgetMessage = 11
	WidgetMsg_ExposedChanged		 WidgetMessage = 12
	WidgetMsg_AcceptChild			 WidgetMessage = 13
	WidgetMsg_LoseChild				 WidgetMessage = 14
	WidgetMsg_AcceptParent			 WidgetMessage = 15
	WidgetMsg_Shown					 WidgetMessage = 16
	WidgetMsg_Hidden				 WidgetMessage = 17
	WidgetMsg_DescriptorChanged		 WidgetMessage = 18
	WidgetMsg_PropertyChanged		 WidgetMessage = 19
	WidgetMsg_MouseWheel			 WidgetMessage = 20
	WidgetMsg_CursorAdjust			 WidgetMessage = 21
	WidgetMsg_CloseButtonPushed		 WidgetMessage = 1200
	WidgetMsg_PushButtonPressed	     WidgetMessage = 1300
	WidgetMsg_UserStart				 WidgetMessage = 10000
)

var callbacks = make(map[C.XPWidgetID]WidgetCallback)

func CreateWidget(
	left,
	top,
	right,
	bottom int,
	isVisible bool,
	descriptor string,
	isRoot bool,
	widgetId WidgetID,
	class WidgetClass) WidgetID {
	cDescriptor := C.CString(descriptor)
	defer C.free(unsafe.Pointer(cDescriptor))
	return WidgetID(C.XPCreateWidget(
			C.int(left),
			C.int(top),
			C.int(right),
			C.int(bottom),
			C.int(utilities.FromBoolToInt(isVisible)),
			cDescriptor,
			C.int(utilities.FromBoolToInt(isRoot)),
			C.XPWidgetID(widgetId),
			C.int(class)))
}

func SetWidgetProperty(widgetId WidgetID, property WidgetPropertyID, value int)  {
	C.XPSetWidgetProperty(C.XPWidgetID(widgetId), C.int(property), C.longlong(value))
}

//export widgetCallback
func widgetCallback(
	inMessage C.XPWidgetMessage,
	widgetId C.XPWidgetID,
	param1 C.intptr_t,
	param2 C.intptr_t) C.int {

	if callback, ok := callbacks[widgetId]; ok {
		return C.int(callback(WidgetMessage(inMessage), WidgetID(widgetId), int(param1), int(param2)))
	}

	return C.int(0)
}

func AddWidgetCallback(widgetId WidgetID, callback WidgetCallback) {
	if callback != nil{
		if _, isExists := callbacks[C.XPWidgetID(widgetId)]; !isExists {
			callbacks[C.XPWidgetID(widgetId)] = callback
			C.XPAddWidgetCallback(C.XPWidgetID(widgetId), C.XPWidgetFunc_t(unsafe.Pointer(C.widgetCallback)))
		}
	}
}

func HideWidget(widgetId WidgetID) {
	C.XPHideWidget(C.XPWidgetID(widgetId))
}

/*
This class destroys a widget. Pass in the ID of the widget to kill.
If you pass 1 for inDestroyChilren, the widget’s children will be destroyed first,
then this widget will be destroyed. (Furthermore, the widget’s children will be
destroyed with the inDestroyChildren flag set to 1, so the destruction will recurse
down the widget tree.) If you pass 0 for this flag, the child widgets will simply end
up with their parent set to 0.
 */
func DestroyWidget(widgetId WidgetID, isDestroyChildren bool) {
	cDestroyChildren := 0

	if isDestroyChildren {
		cDestroyChildren = 1
	}

	C.XPDestroyWidget(C.XPWidgetID(widgetId), C.int(cDestroyChildren))
}

/*
Returns the window (from the XPLMDisplay API) that backs your widget window.
If you have opted in to modern windows, via a call to XPLMEnableFeature(“XPLM_USE_NATIVE_WIDGET_WINDOWS”, 1),
you can use the returned window ID for display APIs like XPLMSetWindowPositioningMode(),
allowing you to pop the widget window out into a real OS window, or move it into VR.

https://developer.x-plane.com/sdk/XPGetWidgetUnderlyingWindow/
 */
func GetWidgetUnderlyingWindow(widgetId WidgetID) display.WindowID {
	return display.WindowID(C.XPGetWidgetUnderlyingWindow(C.XPWidgetID(widgetId)))
}

// This routine returns the bounding box of a widget in global coordinates.
// Pass NULL for any parameter you are not interested in.
func GetWidgetGeometry(widgetId WidgetID) (left, top, right, bottom int) {
	C.XPGetWidgetGeometry(
		C.XPWidgetID(widgetId),
		(*C.int)(unsafe.Pointer(&left)),
		(*C.int)(unsafe.Pointer(&top)),
		(*C.int)(unsafe.Pointer(&right)),
		(*C.int)(unsafe.Pointer(&bottom)))
	return
}

func GetWidgetDescriptor(widgetId WidgetID, maxDescLength int64) (result int, descriptor string) {
	descriptorBuf := (*C.char)(C.malloc(C.ulonglong(maxDescLength)))
	defer C.free(unsafe.Pointer(descriptorBuf))

	result = int(C.XPGetWidgetDescriptor(
		C.XPWidgetID(widgetId),
		descriptorBuf,
		C.int(maxDescLength)))
	descriptor = C.GoString(descriptorBuf)
	return
}

func SetWidgetDescriptor(widgetId WidgetID, descriptor string)  {
	cString := C.CString(descriptor)
	defer C.free(unsafe.Pointer(cString))
	C.XPSetWidgetDescriptor(C.XPWidgetID(widgetId), cString)
}