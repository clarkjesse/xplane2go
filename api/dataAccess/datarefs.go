package dataAccess

/*
#cgo CFLAGS: -DIBM -DSIMDATA_EXPORTS -DXPLM200=1 -DXPLM210=1 -DXPLM300=1 -DXPLM301=1
#cgo CFLAGS: -I../../SDK/CHeaders
#cgo LDFLAGS: -L../../SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64
#include <XPLM/XPLMDataAccess.h>
#include <stdlib.h>
#include <string.h>
*/
import "C"
import (
	"bitbucket.org/clarkjesse/xplane2go/helpers"
	"github.com/spf13/cast"
	"unsafe"
)

type DataRef C.XPLMDataRef
type DataRefType int

const (
	TypeUnknown    DataRefType = 0
	TypeInt        DataRefType = 1
	TypeFloat      DataRefType = 2
	TypeDouble     DataRefType = 4
	TypeFloatArray DataRefType = 8
	TypeIntArray   DataRefType = 16
	TypeData       DataRefType = 32
)

func FindDataRef(dataRefName string) (DataRef, bool) {
	cDataRefName := C.CString(dataRefName)
	defer C.free(unsafe.Pointer(cDataRefName))
	dataRef := C.XPLMFindDataRef(cDataRefName)
	return DataRef(dataRef), dataRef != nil
}

func CanWriteDataRef(dataRef DataRef) bool {
	return C.XPLMCanWriteDataRef(C.XPLMDataRef(dataRef)) == 1
}

func IsDataRefGood(dataRef DataRef) bool {
	return C.XPLMIsDataRefGood(C.XPLMDataRef(dataRef)) == 1
}

func GetDataRefTypes(dataRef DataRef) DataRefType {
	return DataRefType(C.XPLMGetDataRefTypes(C.XPLMDataRef(dataRef)))
}

func GetIntData(dataRef DataRef) int {
	return int(C.XPLMGetDatai(C.XPLMDataRef(dataRef)))
}

func SetIntData(dataRef DataRef, value int) {
	C.XPLMSetDatai(C.XPLMDataRef(dataRef), C.int(value))
}

func GetFloatData(dataRef DataRef) float32 {
	return float32(C.XPLMGetDataf(C.XPLMDataRef(dataRef)))
}

func SetFloatData(dataRef DataRef, value float32) {
	C.XPLMSetDataf(C.XPLMDataRef(dataRef), C.float(value))
}

func GetDoubleData(dataRef DataRef) float64 {
	return float64(C.XPLMGetDatad(C.XPLMDataRef(dataRef)))
}

func SetDoubleData(dataRef DataRef, value float64) {
	C.XPLMSetDatad(C.XPLMDataRef(dataRef), C.double(value))
}

func GetIntArrayData(dataRef DataRef) []int {
	length := int(C.XPLMGetDatavi(C.XPLMDataRef(dataRef), nil, 0, 0))
	result := make([]int, length)
	C.XPLMGetDatavi(C.XPLMDataRef(dataRef), (*C.int)(unsafe.Pointer(&result[0])), 0, C.int(length))
	return result
}

func SetIntArrayData(dataRef DataRef, value []int) {
	C.XPLMSetDatavi(C.XPLMDataRef(dataRef), (*C.int)(unsafe.Pointer(&value[0])), 0, C.int(len(value)))
}

func GetFloatArrayData(dataRef DataRef) []float32 {
	length := int(C.XPLMGetDatavf(C.XPLMDataRef(dataRef), nil, 0, 0))
	result := make([]float32, length)
	C.XPLMGetDatavf(C.XPLMDataRef(dataRef), (*C.float)(unsafe.Pointer(&result[0])), 0, C.int(length))
	return result
}

func SetFloatArrayData(dataRef DataRef, value []float32) {
	C.XPLMSetDatavf(C.XPLMDataRef(dataRef), (*C.float)(unsafe.Pointer(&value[0])), 0, C.int(len(value)))
}

func GetData(dataRef DataRef) []byte {
	length := int(C.XPLMGetDatab(C.XPLMDataRef(dataRef), nil, 0, 0))
	result := make([]byte, length)
	C.XPLMGetDatab(C.XPLMDataRef(dataRef), unsafe.Pointer(&result[0]), 0, C.int(length))
	return result
}

func SetData(dataRef DataRef, value []byte) {
	C.XPLMSetDatab(C.XPLMDataRef(dataRef), unsafe.Pointer(&value[0]), 0, C.int(len(value)))
}

func GetString(dataRef DataRef) string {
	length := int(C.XPLMGetDatab(C.XPLMDataRef(dataRef), nil, 0, 0))
	valueBuffer := (*C.char)(C.malloc(C.size_t(length)))
	defer C.free(unsafe.Pointer(valueBuffer))
	C.XPLMGetDatab(C.XPLMDataRef(dataRef), unsafe.Pointer(valueBuffer), 0, C.int(length))
	return C.GoString(valueBuffer)
}

func SetString(dataRef DataRef, value string) {
	cValue := C.CString(value)
	defer C.free(unsafe.Pointer(cValue))
	C.XPLMSetDatab(C.XPLMDataRef(dataRef), unsafe.Pointer(cValue), 0, C.int(C.strlen(cValue)))
}

func SetAsBytes(dataRef string, valueBytes []byte) {
	if dataRef, isFound := FindDataRef(dataRef); isFound {
		dataRefType := GetDataRefTypes(dataRef)

	switch dataRefType {
		case TypeInt:
			{
				SetIntData(dataRef, helpers.BytesToInt(valueBytes))
			}

		case TypeFloat:
			{
				SetFloatData(dataRef, helpers.BytesToFloat32(valueBytes))
			}

		case TypeDouble:
			{
				SetDoubleData(dataRef, helpers.BytesToFloat64(valueBytes))
			}

		case TypeIntArray:
			{
				SetIntArrayData(dataRef, helpers.BytesToIntArray(valueBytes))
			}

		case TypeFloatArray:
			{
				SetFloatArrayData(dataRef, helpers.BytesToFloat32Array(valueBytes))
			}

		case TypeData:
			{
				SetString(dataRef, cast.ToString(valueBytes))
			}
		}
	}
}

func GetAsBytes(dataRef string) (valueBytes []byte, isExists bool) {
	if dRef, isFound := FindDataRef(dataRef); isFound {
		isExists = isFound

		dataRefType := GetDataRefTypes(dRef)

		switch dataRefType {
		case TypeInt:
			{
				valueBytes = helpers.ToBytes(GetIntData(dRef))
			}

		case TypeFloat:
			{
				valueBytes =  helpers.ToBytes(GetFloatData(dRef))
			}

		case TypeDouble:
			{
				valueBytes =  helpers.ToBytes(GetDoubleData(dRef))
			}

		case TypeIntArray:
			{
				valueBytes =  helpers.ToBytes(GetIntArrayData(dRef))
			}

		case TypeFloatArray:
			{
				valueBytes =  helpers.ToBytes(GetFloatArrayData(dRef))
			}

		case TypeData:
			{
				valueBytes =  helpers.ToBytes(GetData(dRef))
			}
		}
	}
	return
}

func Set(dataRef string, value interface{}) {
	if dataRef, isFound := FindDataRef(dataRef); isFound {
		/*if !CanWriteDataRef(dataRef) {
			//TODO: rewrite dataref to make writtable
		}*/
		dataRefType := GetDataRefTypes(dataRef)

		switch value.(type) {

		case int, float32, float64:
			{
				switch dataRefType {
				case TypeInt:
					{
						SetIntData(dataRef, cast.ToInt(value))
					}

				case TypeFloat:
					{
						SetFloatData(dataRef, cast.ToFloat32(value))
					}

				case TypeDouble:
					{
						SetDoubleData(dataRef, cast.ToFloat64(value))
					}
				}
			}

		case []int, []float32:
			{
				switch dataRefType {
				case TypeIntArray:
					{
						SetIntArrayData(dataRef, value.([]int))
					}

				case TypeFloatArray:
					{
						SetFloatArrayData(dataRef, value.([]float32))
					}
				}
			}

		case []byte:
			{
				if dataRefType == TypeData {
					SetData(dataRef, value.([]byte))
				}
			}

		case string:
			{
				if dataRefType == TypeData {
					SetString(dataRef, cast.ToString(value))
				}
			}
		}
	}
}

func Get(dataRef string) (value interface{}, isExists bool) {
	if dRef, isFound := FindDataRef(dataRef); isFound {
		isExists = isFound

		dataRefType := GetDataRefTypes(dRef)

		switch dataRefType {
		case TypeInt:
			{
				value = GetIntData(dRef)
			}

		case TypeFloat:
			{
				value = GetFloatData(dRef)
			}

		case TypeDouble:
			{
				value = GetDoubleData(dRef)
			}

		case TypeIntArray:
			{
				value = GetIntArrayData(dRef)
			}

		case TypeFloatArray:
			{
				value = GetFloatArrayData(dRef)
			}

		case TypeData:
			{
				value = GetData(dRef)
			}
		}
	}
	return
}
