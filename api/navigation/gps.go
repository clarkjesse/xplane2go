package navigation

/*
#cgo CFLAGS: -DIBM -DSIMDATA_EXPORTS -DXPLM200=1 -DXPLM210=1 -DXPLM300=1 -DXPLM301=1
#cgo CFLAGS: -I../../SDK/CHeaders
#cgo LDFLAGS: -L../../SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64
#include <XPLM/XPLMNavigation.h>
#include <stdlib.h>
#include <string.h>
*/
import "C"

func GetGPSDestinationType() NavType {
	return NavType(C.XPLMGetGPSDestinationType())
}

func GetGPSDestination() NavRef {
	return NavRef(C.XPLMGetGPSDestination())
}
