package plugins

/*
#include <stdlib.h>
#include <string.h>
*/
import "C"
import (
	"bitbucket.org/clarkjesse/xplane2go/api/logging"
	"bitbucket.org/clarkjesse/xplane2go/api/processing"
	"bitbucket.org/clarkjesse/xplane2go/api/utilities"
	"fmt"
	"unsafe"
)

const (
	PluginStart   PluginState = 0
	PluginEnable  PluginState = 1
	PluginDisable PluginState = 2
	PluginStop    PluginState = 3
)

type PluginStateCallback func(state PluginState, plugin *XPlanePlugin)
type PluginState int
type XPlanePlugin struct {
	id                 PluginId
	name               string
	signature          string
	description        string
	messageHandler     MessageHandler
	flightLoop         processing.FlightLoopFunc
	flightLoopInterval float32
	errorCallback      utilities.ErrorCallback
	stateCallback      PluginStateCallback
}

func NewPlugin(name, signature, description string) *XPlanePlugin {
	logging.PluginName = name
	plugin = &XPlanePlugin{NO_PLUGIN_ID, name, signature, description, nil, nil, 1.0, nil, nil}

	logging.Info(fmt.Sprintf("%v is initialized successfully", name))
	logging.Info(fmt.Sprintf("signature: %v", signature))
	logging.Info(fmt.Sprintf("description: %v", description))
	return plugin
}

var (
	plugin *XPlanePlugin
)

func (pRef *XPlanePlugin) GetId() PluginId {
	if pRef.id == -NO_PLUGIN_ID {
		pRef.id = GetMyId()
	}
	return pRef.id
}

func (pRef *XPlanePlugin) GetName() string {
	return pRef.name
}

func (pRef *XPlanePlugin) GetDescription() string {
	return pRef.description
}

func (pRef *XPlanePlugin) GetSignature() string {
	return pRef.signature
}

func (pRef *XPlanePlugin) GetMessageHandler() MessageHandler {
	return pRef.messageHandler
}

func (pRef *XPlanePlugin) SetMessageHandler(handler MessageHandler) {
	pRef.messageHandler = handler
}

func (pRef *XPlanePlugin) SetFlightLoopFunc(flightLoopFunc processing.FlightLoopFunc, interval float32) {
	pRef.flightLoop = flightLoopFunc
	pRef.flightLoopInterval = interval
}

func (pRef *XPlanePlugin) SetErrorCallback(callback utilities.ErrorCallback) {
	pRef.errorCallback = callback
}

func (pRef *XPlanePlugin) SetPluginStateCallback(callback PluginStateCallback) {
	pRef.stateCallback = callback
}

func (pRef *XPlanePlugin) onStart(name, sig, desc *C.char) {
	copyStringToCPointer(pRef.name, name)
	copyStringToCPointer(pRef.signature, sig)
	copyStringToCPointer(pRef.description, desc)
	if pRef.errorCallback != nil {
		utilities.SetErrorCallback(pRef.errorCallback)
	}
	if pRef.flightLoop != nil {
		processing.RegisterFlightLoopCallback(pRef.flightLoop, pRef.flightLoopInterval, pRef)
	}
}

func copyStringToCPointer(text string, target *C.char) {
	cMsg := C.CString(text)
	defer C.free(unsafe.Pointer(cMsg))
	C.strcpy(target, cMsg)
}

func (pRef *XPlanePlugin) String() string {
	return fmt.Sprintf("%v (singature: %v, id: %v)", pRef.GetName(), pRef.GetSignature(), pRef.GetId())
}

//export XPluginReceiveMessage
func XPluginReceiveMessage(pluginId C.int, messageId C.int, messageData unsafe.Pointer) {
	if plugin.messageHandler != nil {
		plugin.messageHandler(Message{PluginId: PluginId(pluginId), MessageId: MessageId(messageId), Data: messageData})
	}
}

//export XPluginStart
func XPluginStart(outName *C.char, outSig *C.char, outDesc *C.char) int {
	plugin.onStart(outName, outSig, outDesc)
	if plugin.stateCallback != nil {
		plugin.stateCallback(PluginStart, plugin)
	}
	return 1
}

//export XPluginEnable
func XPluginEnable() int {
	if plugin.stateCallback != nil {
		logging.Info("Enabling...")
		plugin.stateCallback(PluginEnable, plugin)
		logging.Info("Enabled")
	}
	return 1
}

//export XPluginDisable
func XPluginDisable() {
	if plugin.stateCallback != nil {
		plugin.stateCallback(PluginDisable, plugin)
	}
}

//export XPluginStop
func XPluginStop() {
	if plugin.stateCallback != nil {
		plugin.stateCallback(PluginStop, plugin)
	}
}
