package helpers

import "github.com/kelindar/binary"

func ToBytes(value interface{}) []byte {
	result, err := binary.Marshal(value)
	if err != nil {
		return []byte{}
	}
	return result
}

func BytesToInt(value []byte) int {
	var result int
	err := binary.Unmarshal(value, &result)
	if err != nil {
		return 0
	}
	return result
}

func BytesToFloat32(value []byte) float32 {
	var result float32
	err := binary.Unmarshal(value, &result)
	if err != nil {
		return 0
	}
	return result
}

func BytesToFloat64(value []byte) float64 {
	var result float64
	err := binary.Unmarshal(value, &result)
	if err != nil {
		return 0
	}
	return result
}

func BytesToIntArray(value []byte) []int {
	var result []int
	err := binary.Unmarshal(value, &result)
	if err != nil {
		return []int{}
	}
	return result
}

func BytesToFloat32Array(value []byte) []float32 {
	var result []float32
	err := binary.Unmarshal(value, &result)
	if err != nil {
		return []float32{}
	}
	return result
}
